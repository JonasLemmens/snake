import curses
from random import randint
import sys

### Boilerplate code
# Setup scherm
curses.initscr()
window=curses.newwin(20,60,0,0)
# Pijltjes gebruiken
window.keypad(1)
# Geen andere inputchar
curses.noecho()
curses.curs_set(0)
# Border van window zetten
window.border(0)
# Wacht niet op user input
window.nodelay(1)

class Snake():
    """
    De klasse Slang abstraheert de slang -- ze heeft een aantal cellen, een
    staart, een hoofd, een lengte, ...
    """

    def __init__(self):
        # Initialiseer de positie van de slang
        self.snake = [(4, 10), (4, 9), (4, 8)]
        self.vorige_staart = (4, 8)
        self.error = ''

    @property
    def hoofd(self):
        # Wat is het hoofd van de slang?
        self.error = ''
        return self.vorige_staart

    @property
    def lengte(self):
        self.error = ''
        return len(self.snake)

    @property
    def staart(self):
        self.error = ''
        return self.snake[-1]

    @property
    def lichaam(self):
        return self.snake

    def ga(self, richting):
        """ Ga in de richting dat opgegeven wordt.

        Richting is ofwel links, rechts, boven, of onder.
        """
        # Ga in de richting dat opgegeven wordt
        if richting not in ['links', 'rechts', 'boven', 'onder']:
            # Doe niks als de richting niet gekend is.
            return

        nieuwe_x = self.snake[0][1]
        nieuwe_y = self.snake[0][0]

        if richting == 'links':
            self.error = 'Not implemented: ga rechts'
        elif richting == 'rechts':
            self.error = 'Not implemented: ga rechts'
        elif richting == 'boven':
            self.error = 'Not implemented: ga boven'
        else:
            self.error = 'Not implemented: ga onder'
        self.snake.insert(0, (nieuwe_y, nieuwe_x))

    def schuif_op(self):
        self.vorige_staart = self.snake.pop()

    def raakt_zichzelf(self):
        if self.snake[0] in self.snake[1:]:
            return True
        else:
            return False

    def raakt_rand(self):
        if self.snake[0][0] <= 0 or 19 <= self.snake[0][0]:
            return True
        elif self.snake[0][1] <= 0 or 59 <= self.snake[0][1]:
            return True
        return False

# Aanmaken van slang
snake = Snake()
# Aanmaken van eerste stuk eten.
food = (10,20)
window.addch(food[0],food[1],'#')

score = 0
ESC = 27
key = curses.KEY_RIGHT
error = ''

### Gameloop
while key != ESC:
    if snake.error:
        error = snake.error
        break

    window.addstr(0,2,'Score '+ str(score)+ ' ')
    # Versnellen adhv de lengte van de slang.
    window.timeout(150 - (snake.lengte)//5 + snake.lengte//10 % 120)

    prev_key=key
    event=window.getch()
    key = event if event != -1 else prev_key

    if key not in [curses.KEY_LEFT, curses.KEY_RIGHT,curses.KEY_UP,curses.KEY_DOWN]:
        key=prev_key

    if key==curses.KEY_DOWN:
        error = 'not implemented: key down'
        break
    if key==curses.KEY_UP:
        error = 'not implemented: key up'
        break
    if key==curses.KEY_RIGHT:
        error = 'not implemented: key right'
        break
    if key==curses.KEY_LEFT:
        error = 'not implemented: key left'
        break

    # If the border is hit, or if the snake touches itself, break.
    if snake.raakt_rand() or snake.raakt_zichzelf():
        break

    # Check if snake eats food.
    if snake.hoofd == food:
        food = (0, 0)
        raise NotImplementedError('Nieuwe locatie van eten')

        window.addch(food[0],food[1],'#')
    else:
        window.addch(snake.vorige_staart[0], snake.vorige_staart[1],' ')
        snake.schuif_op()

    window.addch(snake.hoofd[0], snake.hoofd[1],'*')

curses.endwin()
eindscore = "final score = "+ str(score)
print(eindscore)

if error:
     print(error)

